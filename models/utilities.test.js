const path = require('path');
const db = require('../libs/database');

describe('Models: Utilities', () => {
	const Utility = db.sequelize.import(path.join(__dirname, 'utilities.js'));

	test('utility Model should have only these keys', () => {
		const utilityTestKeys = [
			'id',
			'name',
			'value',
			'avoided_cost_rate',
			'metering_policy',
			'metering_type',
			'deleted',
			'contact_info',
			'address',
			'city',
			'state',
			'zipcode',
			'phone',
			'website',
			'utility_type',
			'areas_served',
			'gt_provider',
			'interconnection_policy',
			'related_documents',
			'in_utilities_list',
			'frequently_used_utility',
			'metering_dates',
			'createdAt',
			'updatedAt'
		];

		const utilityCurrentKeys = Object.keys(Utility.rawAttributes);

		const keysDifference = utilityCurrentKeys.filter(
			value => utilityTestKeys.indexOf(value) === -1
		);

		expect(keysDifference.length).toBe(0);
	});
});
