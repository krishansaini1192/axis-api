const { mergeSchemas } = require('graphql-tools');
const utilitySchema = require('./utility');

module.exports = mergeSchemas({
	schemas: [utilitySchema]
});
